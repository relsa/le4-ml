type ('a, 'b) sum = Left of 'a | Right of 'b;;

(* 1 *)
let fun1 (x, y) (* 引数は'a * 'y型 *) =
  match y with
  | Left i -> Left (x, i)
  | Right i -> Right (x, i)
;;


(* 2 *)
let fun2 (x, y) =
  match (x, y) with
  | (Left x', Left y') -> Left (Left (x', y'))
  | (Left x', Right y') -> Right (Left (x', y'))
  | (Right x', Left y') -> Right (Right (x', y'))
  | (Right x', Right y') -> Left (Right (x', y'))
;;

(* 3 *)
let fun3 (f, g) h  =
  match h with
  | Left h' -> f (h')
  | Right h' -> g (h')
;;

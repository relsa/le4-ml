let round_off (x : float) (dplace : int) = 
  let exp = float (dplace - 1) in
  let tmp = floor (x *. 10. ** exp +. 0.5) in
  tmp *. 0.1 ** exp
;;
(*
  小数第n位で四捨五入する関数．
  x: 対象となる数，dplace: 小数第何位で四捨五入するかを示す整数

  以下に定義する関数で用いる．
  [実行例]
  正しく四捨五入できていることを確かめる．
  # round_off 1.25 1;;
  - : float = 1.
  # round_off 1.25 2;;
  - : float = 1.3
*)


(* 1 *)
let exchange_USD_to_JPY (usd : float) = 
  let rate = 111.12 in
  let jpy = usd *. rate in
  int_of_float (round_off jpy 1)
;;
(*
  USDからJPYへの両替をする関数
  [実行例]
  # exchange_USD_to_JPY 1.;;
  - : int = 111
  # exchange_USD_to_JPY 5.;;
  - : int = 556 (555.6の四捨五入)
*)


(* 2 *)
let exchange_JPY_to_USD (jpy : int) =
  let rate = 111.12 in
  let usd = float jpy /. rate in
  round_off usd 2
;;
(*
  JPYからUSDへの両替をする関数
  [実行例]
  # exchange_JPY_to_USD 112;;
  - : float = 1.
  # exchange_JPY_to_USD 600;;
  - : float = 5.4
 *)


(* 3 *)
let announce_ex_USD_to_JPY (usd : float) =
  
  let jpy = exchange_USD_to_JPY usd in
  string_of_float usd ^ " dollars are " ^ string_of_int jpy ^ " yen."
;;
(*
  USDを受け取ってJPYに両替し，文字列を返す関数
  [実行例]
  # announce_ex_USD_to_JPY 1.;;
  - : string = "1. dollars are 111 yen."
# 
*)


(* 4 *)
let capitalize (c : char) =
  let code = int_of_char c in

  (* 'a'は97，'z'は122に相当．
     32を引けば大文字になる． *)
  if 97 <= code && code <= 122 then
    char_of_int (code - 32)
  else
    c
;;
(*
  アルファベットの小文字を大文字にする関数
  [実行例]
  # capitalize 'a';;
  - : char = 'A'
  # capitalize 'z';;
  - : char = 'Z'
  # capitalize '1';;
  - : char = '1'
*)

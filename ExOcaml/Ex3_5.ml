(* 型float * float * float * floatは，
   float型の要素を4つ持った組である．
   例えば，以下のvはこの型を持つ． *)
let v = (1., 2., 3., 4.);;
(* また，以下のように要素を取り出せる．*)
let (v1, v2, v3, v4) = v;;


(* 型(float * float) * (float * float)は
   float型のペアを要素に持つペアである．
   例えば，以下のwはこの型を持つ．*)
let w = ((5., 6.), (7., 8.));;
(* また，以下のように要素を取り出せる．*)
let ((w1, w2), (w3, w4)) = w;;

(*
  [実行結果]
  # #use "Ex3_5.ml";;
  val v : float * float * float * float = (1., 2., 3., 4.)
  val v1 : float = 1.
  val v2 : float = 2.
  val v3 : float = 3.
  val v4 : float = 4.
  val w : (float * float) * (float * float) = ((5., 6.), (7., 8.))
  val w1 : float = 5.
  val w2 : float = 6.
  val w3 : float = 7.
  val w4 : float = 8.
*)

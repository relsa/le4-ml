(* 1 *)
let rec downto0 n =
  if n = 0 then
    [0]
  else
    n :: downto0 (n - 1)
;;

(* [説明] *)
(* 再帰的にnから0までconsしていくだけである。 *)

(* [実行例] *)
(* # downto0 10;;                                    *)
(* - : int list = [10; 9; 8; 7; 6; 5; 4; 3; 2; 1; 0] *)


let patterns = [
  (1000, "M");
  (900, "CM");
  (500, "D");
  (400, "CD");
  (100, "C");
  (90, "XC");
  (50, "L");
  (40, "XL");
  (10, "X");
  (9, "IX");
  (5, "V");
  (4, "IV");
  (1, "I")
] (* 実行用 *)

(* 2 *)
let roman pat n =
  let rec iter p n s =
    if n > 0 then
      let (num, r_num) = List.hd p in
      let q = n / num in
      if q = 0 then
	iter (List.tl p) n s
      else
	iter p (n - num) (s ^ r_num)
    else
      s
  in
  iter pat n ""
;;

(* [説明] *)
(* 局所関数iterを実装することでromanを反復的に実装した。                *)
(* iterはローマ数字表現パターンが格納されたリストp,                     *)
(* 整数値n, 返り値となる文字列sを保持する。                             *)
(* n = 0ならばsを返す。                                                 *)
(* n > 0ならば、pの先頭のパターンとマッチングを行う。                   *)
(* 該当する場合、sにローマ数字を結合し、nから該当した数を引き反復する。 *)
(* 該当しない場合、リストからそのパターンを削除し、反復する。           *)
(* パターンは数の大きい順に並んでいるため、整合性は保たれる。           *)

(* [実行例] *)
(* # roman patterns 2013;; *)
(* - : string = "MMXIII"   *)
(* # roman patterns 1192;; *)
(* - : string = "MCXCII"   *)


(* 3 *)
let concat list = List.fold_right List.append list [];;

(* [説明] *)
(* fold_rightを用いた。                           *)
(* 末尾の要素からappendが実行され、畳み込まれる。 *)

(* [実行例] *)
(* # concat [[0; 1; 2]; [3; 4]; [5]];;      *)
(* - : int list = [0; 1; 2; 3; 4; 5]        *)
(* # concat [[[0]; [1; 2]]; [[3]]];;        *)
(* - : int list list = [[0]; [1; 2]; [3]]   *)


(* 4 *)
let zip l1 l2 =
  let rec iter l1 l2 ret = 
    match (l1, l2) with
      ([], _) -> ret
    | (_, []) -> ret
    | (h1 :: r1, h2 :: r2) -> iter r1 r2 ((h1, h2) :: ret) in
  List.rev (iter l1 l2 [])
;;

(* [説明] *)
(* 局所関数iterを実装することで反復的に実装した。                   *)
(* iterはパターンマッチを行う。                                     *)
(* 引数のリストのうち、いずれかが空になったら結果を返す。           *)
(* いずれも空でなかったら、それぞれの先頭要素のペアを結果に結合し、 *)
(* 残りのリストについて反復を行う。                                 *)
(* iterが結果として返すリストは逆順になっているので、               *)
(* 最後にrevをかけて反転させる。                                    *)

(* [実行例] *)
(* #  zip ["yaruki"; "sonoki"; "daisuki"] [1; 2; 3; 4; 5];;                 *)
(* - : (string * int) list = [("yaruki", 1); ("sonoki", 2); ("daisuki", 3)] *)

(* 5 *)
let filter prod list =
  let rec iter l ret =
    if l = [] then
      ret
    else
      if prod (List.hd l) then
	iter (List.tl l)  ((List.hd l) :: ret)
      else
	iter (List.tl l) ret
  in
  List.rev (iter list [])
;;

(* [説明] *)
(* 局所関数iterを実装することで反復的に実装した。 *)
(* リストの先頭から順に見ていき、                 *)
(* 条件を満たしていれば、結果にconsし、           *)
(* 満たしていなければ、読み飛ばしている。         *)

(* [実行例] *)
(* # filter (fun x -> x > 0) [-5; -3; 0; 3; 5];; *)
(* - : int list = [3; 5]                         *)

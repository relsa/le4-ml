let prodMatVec (mat, vec) =
  let ((a, b), (c, d)) = mat and
      (v1, v2) = vec in
  (a *. v1 +. b *. v2, c *. v1 +. d *. v2)
;;

(*
  2*2実数行列 mat = a b
                    c d と
  縦ベクトル = v1
               v2 をそれぞれ
  ((a, b), (c, d)), (v1, v2)の形で受け取り，積を計算する．

  [実行例]
  # prodMatVec(((1., 0.), (0., 1.)), (3., 4.));;
  - : float * float = (3., 4.)
  # prodMatVec(((3., 2.), (1., 5.)), (3., 4.));;
  - : float * float = (17., 23.)
*)  

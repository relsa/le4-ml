let uncurry f (x, y) = f x y;;

(* [説明] *)

(* カリー化された2変数関数の型は *)
(* 'a -> 'b -> 'c *)
(* である。 *)
(* よって、uncurry関数の型は *)
(* ('a -> 'b -> 'c) -> 'b * 'c -> c *)
(* と考えられる。 *)
(* 上のような定義にすると、 *)
(* カリー化された関数と引数のペアを受取り、 *)
(* 受け取った引数に、受け取った関数を適用する関数となる。 *)
(* これを部分適用させると受け取った関数を *)
(* uncurry化することができる。 *)


(* [実行例] *)

(* # curried_avg;;                                         *)
(* - : float -> float -> float = <fun>                     *)
(* # let uncurry f (x, y) = f x y;;                        *)
(* val uncurry : ('a -> 'b -> 'c) -> 'a * 'b -> 'c = <fun> *)
(* # let avg = uncurry curried_avg in avg (4., 5.3);;      *)
(* - : float = 4.65                                        *)
(* # uncurry curried_avg;;                                 *)
(* - : float * float -> float = <fun>                      *)

let integral f a b =
  let n = 100000 in
  let sigma = (b -. a) /. float n in
  let rec iter sum i =
    let f_i = float i in
    let trapezoid =
      (f (a +. (f_i -. 1.) *. sigma) +. f (a +. f_i *. sigma)) *. sigma /. 2. in
    if i = n + 1 then
      sum
    else
      iter (sum +. trapezoid) (i+1) in
  iter 0. 1
;;

(* 積分を計算する関数 *)
(* f: 適用する関数  *)
(* a, b: 積分範囲(a < b) *)
(* -------------------------------- *)
(* 今回は領域を100000分割し、台形近似を行った。 *)
(* 局所関数iterで台形の加算を行っている。 *)
(* 台形の和はsumに、繰り返し回数はiに保たれる。 *)
(* integralはiterに初期値sum = 0., i = 1を与えて呼び出している。 *)

(* -------------------------------- *)
(* [実行例] *)

(* # integral sin 0. 3.141592;; *)
(* - : float = 1.99999999983527399 *)
(* 実際に計算すると答えは2.0になる。 *)
(* この関数は十分な精度で積分近似できている。 *)

(* # integral (fun x -> x *. x) 0. 1.;; *)
(* - : float = 0.333333333350000149 *)
(* 実際に計算すると答えは0.33333..になる。 *)
(* この関数は十分な精度で積分近似できている。 *)

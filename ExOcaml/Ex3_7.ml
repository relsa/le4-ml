(* 1 *)
let rec pow (x, n) =
  if n = 0 then
    1.
  else
    x *. pow (x, n - 1)
;;
(* x**n = x * x**(n-1)として
   再帰的にべき乗計算を行っている．
   n = 0のとき1.を返す．*)

(* 2 *)
let rec better_pow (x, n) =
  if n = 0 then
    1.
  else if n mod 2 = 1 then
    x *. better_pow (x, n - 1)
  else
    better_pow (x *. x, n / 2)
;;
(* n = 0ならば1.を返し，
   nが奇数ならばx**n = x * x**(n-1)として計算する．
   nが偶数の時，x**n = (x**2)**(n/2)として計算を行なっている．
   この処理により，再帰呼び出しの数は従来より減少し，
   O(logn)の計算量で済む． *)

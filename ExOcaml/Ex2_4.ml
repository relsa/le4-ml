(* for run this program *)
let b1 = true
and b2 = true;;

(* b1 && b2 *)
if b1 then
  b2
else
  false
;;

(* b1 || b2 *)
if b1 then
  true
else
  b2
;;

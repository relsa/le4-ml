let powi (b, e) =
  let rec iter (prod, times) =
    if times = 0 then
      prod
    else
      iter (b *. prod, times - 1) in
  iter(1., e)
;;

(* 
   局所関数iterが反復的に動作する．
   引数prodは積を，timesは残りの反復数を示している．
   そのため，prod, timesの初期値はそれぞれ1., 指数eとなる．
   反復の度にtimesを1減少させ，prodに基数bを掛け，
   timesが0となったとき，iterはこれまでの積prodを返す．
   
   [実行例]
   # powi (2., 10);;
   powi <-- (2., 10)
   powi --> 1024.
   - : float = 1024.
*)

(* 変数3つ版 powi(x, 1, e)で呼び出し    *)
(* ------------------------------------ *)
(* let rec powi (x, prod, times) =      *)
(*   if times = 0 then                  *)
(*     prod                             *)
(*   else                               *)
(*     powi (x, x *. prod, times - 1);; *)

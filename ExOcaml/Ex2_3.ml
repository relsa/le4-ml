(* 1
   not true && false -> true *)
not (true && false);;
(* 単項演算子は二項演算子より優先度が高い． *)

(* 2
   float_of_int int_of_float 5.0 -> 5.0 *)
float_of_int (int_of_float 5.0);;
(* float_of_intが先に適用されて，文法エラーとなる．*)

(* 3
   sin 3.14 /. 2.0 ** 2.0 +. cos 3.14 /. 2.0 ** 2.0 -> 1.0 *)
sin (3.14 /. 2.0) ** 2.0 +. cos (3.14 /. 2.0) ** 2.0;;
(* べき乗演算子の方が，
   除算演算子より優先順位が高い．*)

(* 4
   sqrt 3 * 3 + 4 * 4 -> 5*)
int_of_float (sqrt (float_of_int (3 * 3 + 4 * 4)));;
(* sqrtはfloat->floatの関数であるので，型変換が必要である．
   また，関数適用は中置演算子より優先度が高いため，
   float_of_intの引数にも括弧をつける必要がある．*)

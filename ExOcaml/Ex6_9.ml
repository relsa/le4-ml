(* 'a seq型の定義 *)
type 'a seq = Cons of 'a * (unit -> 'a seq);;

(* 以下、seq操作関数 *)
let rec from n = Cons (n, fun () -> from (n + 1));;

let head (Cons (x, _)) = x;;
let tail (Cons (_, f)) = f ();;

let rec take n s =
  if n = 0 then
    []
  else
    head s :: take (n - 1) (tail s)
;;

let rec nthseq n (Cons (x, f)) =
  if n = 1 then
    x
  else
    nthseq (n - 1) (f ())
;;
(* ----------- *)


let rec sieve (Cons (x, f)) =
  let rec sift n s =
    if head s mod n = 0 then
      sift n (tail s)
    else
      Cons (head s, fun () -> sift n (tail s))
  in
  Cons (x, fun () -> sieve (sift x (f ()) ));;
(* [説明] *)
(* エラトステネスの篩を実装した。                           *)
(* 内部関数siftは整数nとシーケンスsを受取り、               *)
(* sからnの倍数を排除したシーケンスを返す。                 *)
(* sの先頭要素がnで割り切れる場合、                         *)
(* その数を破棄してシーケンスの続きに再度siftを適用する。   *)
(* sの先頭要素がnで割り切れない場合、                       *)
(* その数をConsで結合し、シーケンスの続きにsiftを適用する。 *)

(* [実行例] *)
(* # let primes = sieve (from 2);;                     *)
(* val primes : int seq = Cons (2, <fun>)              *)
(* # take 10 primes;;                                  *)
(* - : int list = [2; 3; 5; 7; 11; 13; 17; 19; 23; 29] *)

(* ----- 学生番号下4桁 + 3000 番目の素数 -----         *)
(* # nthseq (8500 + 3000) primes;;                     *)
(* - : int = 122251                                    *)

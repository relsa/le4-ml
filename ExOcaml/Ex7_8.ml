let rec change = function
  | (_, 0) -> [] (* 両替残金が0 *)
  | ((c :: rest) as coins, total) ->
    if c > total then
      change (rest, total) (* 一番大きい硬貨を除いてリトライ  *)
    else
      begin
	try
	  c :: change (coins, total - c) (* 硬貨を選択して残金を減らす *)
	with
	| Failure "change" -> (* 例外をcatch *)
	  change (rest, total) (* 一番大きい硬貨を除いてリトライ *)
      end
  | _ -> raise (Failure "change") (* 例外をraise *)
;;

(* [実行例] *)
(* 例外が発生しない例                   *)
(* # change ([5; 2], 32);;              *)
(* - : int list = [5; 5; 5; 5; 5; 5; 2] *)

(* 例外が発生する例               *)
(* # change ([5; 2], 16);;        *)
(* - : int list = [5; 5; 2; 2; 2] *)


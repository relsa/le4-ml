let incr r =
  r := !r + 1
;;
(* [説明] *)
(* 参照rに、rに格納された値+1を格納しなおしている。 *)

(* [実行例] *)
(* # let x = ref 3;;                *)
(* val x : int ref = {contents = 3} *)
(* # x;;                            *)
(* - : int ref = {contents = 3}     *)
(* # incr x;;                       *)
(* - : unit = ()                    *)
(* # x;;                            *)
(* - : int ref = {contents = 4}     *)

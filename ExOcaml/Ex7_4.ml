let fact_imp n =
  let i = ref n and
      res = ref 1 in
  while !i > 0 do
    res := !res * !i;
    i := !i - 1;
  done;
  !res
;;

(* [実行例] *)
(* # fact_imp 5;;    *)
(* - : int = 120     *)
(* # fact_imp 10;;   *)
(* - : int = 3628800 *)

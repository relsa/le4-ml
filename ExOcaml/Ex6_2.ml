(* natの定義 *)
type nat = Zero | OneMoreThan of nat;;

(* 準備 *)
let rec add m n =
  match m with
  | Zero -> n
  | OneMoreThan m' -> OneMoreThan (add m' n)
;;



let int_of_nat n =
  let rec count n i =
    match n with
    | Zero -> i
    | OneMoreThan n' -> count n' (i + 1)
  in
  count n 0
;;
(* [説明] *)
(* nat型をint型に変換する関数。                                     *)
(* 局所関数countが、nat型引数nに含まれるOneMoreThanの回数を数える。 *)

(* [実行例] *)
(* # int_of_nat Zero;;                             *)
(* - : int = 0                                     *)
(* # int_of_nat (OneMoreThan (OneMoreThan Zero));; *)
(* - : int = 2                                     *)


let rec mul m n =
  match (m, n) with
  | (Zero, _) -> Zero
  | (_, Zero) -> Zero
  | (OneMoreThan m', n') -> add n' (mul m' n')
;;
(* [説明] *)
(* nat型の掛け算をする関数。                     *)
(* 0 * n = 0                                     *)
(* m * 0 = 0とし、                               *)
(* それ以外の場合、n + (m - 1) * nを計算する。 *)

(* [実行例] *)
(* # let three = (OneMoreThan (OneMoreThan (OneMoreThan Zero))) and *)
(*       two = (OneMoreThan (OneMoreThan Zero)) in                  *)
(*   mul three two;;                                                *)
(* - : nat =                                                        *)
(*       OneMoreThan                                                *)
(*        (OneMoreThan                                              *)
(* 	   (OneMoreThan                                             *)
(* 	    (OneMoreThan                                            *)
(* 	     (OneMoreThan                                           *)
(*            (OneMoreThan Zero)))))                                *)
(* 3 * 2 = 6が正しく計算されている。                                *)


let rec monus m n =
  match (m, n) with
  | (Zero, _) -> Zero
  | (m', Zero) -> m'
  | (OneMoreThan m', OneMoreThan n') -> monus m' n'
;;

(* [説明] *)
(* nat型の引き算をする関数。                     *)
(* 0 - n = 0                                     *)
(* n - 0 = 0とし、                               *)
(* それ以外の場合、(m - 1) - (n - 1)を計算する。 *)

(* [実行例] *)
(* # let three = (OneMoreThan (OneMoreThan (OneMoreThan Zero))) and *)
(*       two = (OneMoreThan (OneMoreThan Zero)) in                  *)
(*   monus three two;;                                              *)
(* - : nat = OneMoreThan Zero                                       *)
(* 3 - 2 = 1が正しく計算されている。                                *)

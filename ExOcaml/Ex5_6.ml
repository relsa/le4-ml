let rec quicker l sorted = 
  match l with
    [] -> sorted
  | x :: xs -> 
    let rec partition left right =
      function
        [] -> quicker left (x :: (quicker right sorted))
      | y :: ys ->
	if x < y then
	  partition left (y :: right) ys
	else
	  partition (y :: left) right ys
    in
    partition [] [] xs
;;


(* [説明] *)
(* 関数quickerは、未ソートリストlが空になったら、     *)
(* ソート済リストsortedを返す。                       *)
(* 局所関数partitionは、lの先頭要素をpivotとし、      *)
(* pivotより値の小さい要素のリストleftと              *)
(* 値の大きい要素のリストrightに分ける。              *)
(* 未ソートリストrightを先にquickerでソートしてやり、 *)
(* ソートされたrightの先頭にpivotを追加する。         *)
(* rightはpivotよりも大きい要素のリストだったので、   *)
(* これもまたソート済みリストとなる。                 *)
(* ソートされたpivot :: rightをsortedとみなし、       *)
(* leftに対しquickerを走らせるとソートが完了する。    *)

(* [実行例] *)
(* # quicker [] [];;           *)
(* - : 'a list = []            *)
(* # quicker [1] [];;          *)
(* - : int list = [1]          *)
(* # quicker [1; 2; 3; 4] [];; *)
(* - : int list = [1; 2; 3; 4] *)
(* # quicker [2; 4; 3; 1] [];; *)
(* - : int list = [1; 2; 3; 4] *)
(* # quicker [4; 3; 2; 1] [];; *)
(* - : int list = [1; 2; 3; 4] *)

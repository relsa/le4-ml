type 'a tree = Lf | Br of 'a * 'a tree * 'a tree;;

let inttree = Br (0,
		  Br (1, Br (2, Lf, Lf), Br (3, Lf, Lf)),
		  Br (4, Br (5, Lf, Lf), Br (6, Lf, Lf)));;
let rec reflect comptree =
  match comptree with
  | Lf -> Lf
  | Br (n, s1, s2) -> Br (n, reflect s2, reflect s1)
;;
(* [説明] *)
(* 葉ならそのまま葉を返し、                                      *)
(* 節点なら、枝を入れ替えて各枝に対しreflectを再帰呼び出しする。 *)

(* [実行例] *)
(* # reflect inttree;;                               *)
(* - : int tree =                                    *)
(*       Br (0,                                      *)
(* 	  Br (4, Br (6, Lf, Lf), Br (5, Lf, Lf)),    *)
(*           Br (1, Br (3, Lf, Lf), Br (2, Lf, Lf))) *)

(* また、任意の二分木tについて以下の恒等式が成り立つ *)
(* preorder (reflect t) = reverse (outorder t)       *)
(* inorder  (reflect t) = reverse (inorder t)        *)
(* outorder (reflect t) = reverse (preorder t)       *)

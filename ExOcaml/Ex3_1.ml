(* 1 *)
let x = 1 in     (* x:1 *)
let x = 3 in     (* x:3 *)
let x = x + 2 in (* x:5 *)
x * x
;;
(* 5 * 5 = 25 *)


(* 2 *)
let x = 2 
and y = 3 in      (* x:2, y:3 *)
(let y = x and
     x = y + 2 in (* x:5, y:2 *)
 x * y)           (* x:2, y:3 *)
+ y
;;
(* (5 * 2) + 3 = 13 *)


(* 3 *)
let x = 2 in     (* x:2 *)
let y = 3 in     (* x:2, y:3 *)
let y = x in     (* x:2, y:2 *)
let z = y + 2 in (* x:2, y:2, z:4 *)
x * y * z
;;
(* 2 * 2 * 4 *)

let rec repeat f n x =
  if n > 0 then
    repeat f (n - 1) (f x)
  else
    x
;;

let fib n =
  let (fibn, _) = 
    repeat (fun (x, y) -> (y, x + y)) n (0, 1) in
  fibn
;;

(* [説明] *)

(* fib 0の値は0とする。 *)
(* ペア(0, 1)に、 *)
(* 関数fun (x, y) -> (y, x + y)を繰り返し適用することで、 *)
(* ペアの頭にはその時点でのフィボナッチ数が格納される。 *)
(* それをマッチングし、取り出すことでフィボナッチ数を求める関数を実装できる。 *)

(* --------------- *)

(* [実行例]     *)

(* # fib 0;;    *)
(* - : int = 0  *)
(* # fib 5;;    *)
(* - : int = 5  *)
(* # fib 10;;   *)
(* - : int = 55 *)

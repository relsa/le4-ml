let geo_mean (x, y) = sqrt(x *. y);;
(* 実数値のペア(x, y)を受け取り，
   xとyの相乗平均を返す． 

   [実行例]
   # geo_mean (5., 3.);;
   - : float = 3.87298334620741702
*)

(* 1 *)
let rec gcd (n, m) =
  let rest = n mod m in
  if rest = 0 then
    m
  else
    gcd (m, rest)
;;
(*
  ユークリッド互除法により再帰的に実装した．
  m <= n を仮定している．
  [実行例]
  # gcd (555, 333);;
  - : int = 111
*)


(* 2 *)
let rec comb (n, m) =
  if m = 0 || n = m then
    1
  else
    comb (n - 1, m) + comb (n - 1, m - 1)
;;
(*
  再帰関数である．
  0 <= m <= n を仮定している
  [実行例]
  # comb (10, 3);;
  - : int = 120
*)  


(* 3 *)
let fib_iter n =
  let rec iter (next, curr, cnt) =
    if cnt = 0 then
      curr
    else
      iter (next + curr, next, cnt - 1) in
  iter (1, 0, n)
;;
(* 
   末尾再帰関数である．
   局所関数iterは，フィボナッチ数next, currと
   残りの反復回数cntを保持し，更新する．
   [実行例]
   # fib_iter 10;;
   - : int = 55
*)   


(* 4 *)
let max_ascii (s : string) =
  let len = String.length s in
  let rec iter (ret, i) =
    if i = len then
      ret
    else
      let c = s.[i] in
      if c > ret then
	iter (c, i + 1)
      else
	iter (ret, i + 1) in
  iter (s.[0], 0)
;;
(*
  末尾再帰で実装した．
  局所関数iterはその時点での暫定返り値retと
  注目文字のインデックスiを保持，更新する．
  iterにs.[0], 0を与えてやると，max_asciiは動作する．

  [実行例]
  # max_ascii "objectivecaml";;
  - : char = 'v'  
  # max_ascii "anz";;
  - : char = 'z'
*)


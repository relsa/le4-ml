open Syntax;;

exception Error of string;;

let err s = raise (Error s);;

(* type Environment *)

type tyenv = ty Environment.t;;
type subst = (tyvar * ty) list;;

let rec subst_type (s : subst) (t : ty) : ty =
  let rec identify_type (sub_s : subst) (v : tyvar) : ty =
    match sub_s with
    | [] -> TyVar v
    | (v', t') :: rest ->
      if v' = v then
        subst_type rest t'
      else
        identify_type rest v in

  match t with
  | TyInt -> TyInt
  | TyBool -> TyBool
  | TyVar v -> identify_type s v
  | TyFun (t1, t2) -> TyFun (subst_type s t1, subst_type s t2)
;;

let rec unify (l : (ty * ty) list) : subst = (* Ex4.4 *)
  let rec override_var (v : tyvar) (t : ty) (subl : (ty * ty) list) =
    (* 型変数の書き換えを行う *)
    let rec iter t' =
      (* 入れ子になっている部分も全探索して書き換える *)
      match t' with
      | TyInt ->
        TyInt
      | TyBool ->
        TyBool
      | TyVar v' ->
        if v = v' then
          t
        else
          TyVar v'
      | TyFun (a, b) ->
        TyFun (iter a, iter b) in

    match subl with
    | [] ->
      []
    | (t1, t2) :: rest ->
      (iter t1, iter t2) :: (override_var v t rest) in

  match l with
  | [] ->
    []
  | (t1, t2) :: rest ->
    if t1 = t2 then
      unify rest
    else
      begin
        match t1, t2 with
        | TyVar a, t ->
          if MySet.member a (freevar_ty t) then
            err ("failed unification")
          else
            (a, t) :: unify (override_var a t rest)
        | t, TyVar a ->
          if MySet.member a (freevar_ty t) then
            err ("failed unification")
          else
            (a, t) :: unify (override_var a t rest)
        | TyFun (t11, t12), TyFun (t21, t22) ->
          unify ((t11, t21) :: (t12, t22) :: rest)
        | _, _ ->
          err ("failed unification")
      end
;;

let eqs_of_subst (s : subst) : (ty * ty) list =
  List.map (fun (tv, t) -> (TyVar tv, t)) s
;;

let subst_eqs (s : subst) (eqs : (ty * ty) list) : (ty * ty) list =
  List.map (fun (t1, t2) -> ((subst_type s t1), (subst_type s t2))) eqs
;;

let ty_prim op ty1 ty2 =
  match op with
  | Plus ->
	  ([(ty1, TyInt); (ty2, TyInt)], TyInt)
  | Mult ->
	  ([(ty1, TyInt); (ty2, TyInt)], TyInt)
  | Lt ->
    ([(ty1, TyInt); (ty2, TyInt)], TyBool)
  | And ->
    ([(ty1, TyBool); (ty2, TyBool)], TyBool)
  | Or ->
    ([(ty1, TyBool); (ty2, TyBool)], TyBool)
;;

let rec ty_exp tyenv = function
  | Var x ->
    begin
      try ([], Environment.lookup x tyenv) with
      | Environment.Not_bound ->
	      err ("Variable not bound: " ^ x)
    end
  | ILit _ ->
    ([], TyInt)
  | BLit _ ->
    ([], TyBool)
  | BinOp (op, exp1, exp2) ->
    let (s1, ty1) = ty_exp tyenv exp1 in
    let (s2, ty2) = ty_exp tyenv exp2 in
    let (eqs3, ty) = ty_prim op ty1 ty2 in
    let eqs = (eqs_of_subst s1) @ (eqs_of_subst s2) @ eqs3 in
    let s3 = unify eqs in
    (s3, subst_type s3 ty)
  | IfExp (exp1, exp2, exp3) ->
    let (s1, ty1) = ty_exp tyenv exp1 in
    let (s2, ty2) = ty_exp tyenv exp2 in
    let (s3, ty3) = ty_exp tyenv exp3 in
    let (eqs4, ty) = ([(ty2, ty3); (ty1, TyBool);], ty2) in
    let eqs = (eqs_of_subst s1) @ (eqs_of_subst s2) @ (eqs_of_subst s3) @ eqs4 in
    let s = unify eqs in
    (s, subst_type s ty)
  | LetExp (id_exp_list, exp) ->        (* let文拡張に伴う変更あり *)
    let rec iter list tyenv tyenv' =
      match list with
      | [] ->
        ([], tyenv')
      | (id, exp) :: cdr ->
        let (s', t) = ty_exp tyenv exp in
        let newenv = Environment.extend id t tyenv' in
        let (next_s, next_e) = iter cdr tyenv newenv in
        let eqs = (eqs_of_subst s') @ (eqs_of_subst next_s) in
        let s = unify eqs in
        (s, next_e) in

    let (s1, newenv) = iter id_exp_list tyenv tyenv in
    let (s2, ty) = ty_exp newenv exp in
    let eqs = (eqs_of_subst s1) @ (eqs_of_subst s2) in
    let s3 = unify eqs in
    (s3, subst_type s3 ty)
  | FunExp (id, exp) ->
    let domty = TyVar (fresh_tyvar ()) in
    let s, ranty = ty_exp (Environment.extend id domty tyenv) exp in
    (s, TyFun (subst_type s domty, ranty))
  | AppExp (exp1, exp2) ->
    let (s1, ty1) = ty_exp tyenv exp1 in
    let (s2, ty2) = ty_exp tyenv exp2 in
    let domty = TyVar (fresh_tyvar ()) in
    let (eqs3, ty) = ([(ty1, TyFun (ty2, domty))], domty)in
    let eqs = (eqs_of_subst s1) @ (eqs_of_subst s2) @ eqs3 in
    let s = unify eqs in
    (s, subst_type s ty)
  | _ ->
    err ("Not Implemented")
;;

let ty_decl tyenv = function
  | Exp e -> ty_exp tyenv e
  (* | LetDecl list -> *)
  | _ -> err ("Not Implemented")
;;

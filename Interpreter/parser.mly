%{
  open Syntax
%}

%token LPAREN RPAREN SEMISEMI
%token PLUS MULT LT
%token IF THEN ELSE TRUE FALSE

%token LAND LOR // Ex3.3
%token LET IN EQ // Ex3.4
%token AND // Ex3.7
%token RARROW FUN // Ex3.8
%token REC // Ex3.14
%token MATCH WITH CONS BAR LBR RBR // Ex3.16
%token SEMI // Ex3.17

%token <int> INTV
%token <Syntax.id> ID

%start toplevel
%type <Syntax.program> toplevel
%%

toplevel :
    Expr SEMISEMI { Exp $1 }
  | LET LetDecl SEMISEMI { LetDecl $2 } // Ex3.5
  | LET REC ID EQ FUN ID RARROW Expr SEMISEMI { RecDecl ($3, $6, $8) } // Ex3.14

LetDecl : // Ex3.5 let ... let ...の処理
  | LetVar LET LetDecl { $1 :: $3 }
  | LetVar  { $1 :: [] }

LetVar :
  | ID EQ Expr AND LetVar { ($1, $3) :: $5 }
  | ID EQ Expr { ($1, $3) :: [] }
  | ID Params EQ Expr AND LetVar { ($1, List.fold_right cleate_funexp $2 $4) :: $6 }
  | ID Params EQ Expr { ($1, List.fold_right cleate_funexp $2 $4) :: [] }

Expr :
    IfExpr { $1 }
  | LetExpr { $1 }
  | LOExpr { $1 } // Ex3.3
  | FunExpr { $1 } // Ex3.8
  | LetRecExpr { $1 } // Ex3.14
  | MatchExpr { $1 } // Ex3.15


MatchExpr : // Ex3.15
  | MATCH Expr WITH LBR RBR RARROW Expr BAR ID CONS ID RARROW Expr
      { MatchExp ($2, $7, $9, $11, $13) }

// FunExpr : // Ex3.8
//   | FUN ID RARROW Expr { FunExp ($2, $4) }

FunExpr :
  | FUN Params RARROW Expr { List.fold_right cleate_funexp $2 $4 }

Params : // Ex3.10
  | ID Params { $1 :: $2 }
  | ID { $1 :: [] }

LetRecExpr : // Ex3.14
  | LET REC ID EQ FUN ID RARROW Expr IN Expr
      { LetRecExp ($3, $6, $8, $10) }

// LetExpr : // Ex3.4
//     LET ID EQ Expr IN Expr { LetExp ($2, $4, $6) }

LetExpr : // Ex3.7で変更
  | LET LetVar IN Expr { LetExp ($2, $4) }

LOExpr : // Ex3.3
    LAExpr LOR LOExpr { BinOp (Or, $1, $3) }
  | LAExpr { $1 }

LAExpr : // Ex3.3
    LTExpr LAND LAExpr { BinOp (And, $1, $3) }
  | LTExpr { $1 }

// LTExpr : 
//     PExpr LT PExpr { BinOp (Lt, $1, $3) }
//   | PExpr { $1 }

LTExpr : 
    PExpr LT ConsExpr { BinOp (Lt, $1, $3) }
  | ConsExpr { $1 }

ConsExpr : // Ex3.15, Ex3.16
  | ConsExpr CONS ConsExpr { ConsExp ($1, $3) }
  | LBR ListElems RBR { List.fold_right cleate_consexp $2 (NilLit ()) }
  | PExpr { $1 }

ListElems : // Ex3.16
  | Expr SEMI ListElems { $1 :: $3 }
  | Expr { $1 :: [] }
  | Expr SEMI { $1 :: [] }

PExpr :
  | PExpr PLUS MExpr { BinOp (Plus, $1, $3) }
  | MExpr { $1 }

MExpr :
  | MExpr MULT AppExpr { BinOp (Mult, $1, $3) }
  | AppExpr { $1 }

AppExpr : // Ex3.8
  | AppExpr AExpr { AppExp ($1, $2) }
  | AExpr { $1 }

AExpr :
    INTV { ILit $1 }
  | TRUE { BLit true }
  | FALSE { BLit false }
  | ID { Var $1 }
  | LPAREN Expr RPAREN { $2 }
  | LBR RBR { NilLit () } // Ex3.15

IfExpr :
    IF Expr THEN Expr ELSE Expr { IfExp ($2, $4, $6) }

   

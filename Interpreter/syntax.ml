(* ML interpreter / type reconstruction *)
type id = string;;

type binOp = Plus | Mult | Lt |
             And | Or                   (* Ex3.3 *)
;;

type tyvar = int;;

type ty =                               (* Ex4.1 *)
  TyInt 
| TyBool
| TyVar of tyvar
| TyFun of ty * ty
;;

let pp_ty ty =                          (* Ex4.2 *)
  let rec string_of_type ty =
    match ty with
    | TyInt ->
      "int"
    | TyBool ->
      "bool"
    | TyVar t ->
      "'" ^ Char.escaped (char_of_int (97 + t))
    | TyFun (tv, te) ->
      "(" ^ (string_of_type tv) ^ " -> " ^ (string_of_type te) ^ ")" in
  print_string (string_of_type ty)
;;

type exp =
  Var of id
| ILit of int
| BLit of bool
| BinOp of binOp * exp * exp
| IfExp of exp * exp * exp
| LetExp of (id * exp) list * exp       (* Ex3.4 *)
| FunExp of id * exp                    (* Ex3.8, Ex3.10 *)
| AppExp of exp * exp                   (* Ex3.8 *)
| LetRecExp of id * id * exp * exp      (* Ex3.14 *)
| MatchExp of exp * exp * id * id * exp (* Ex3.16 *)
| NilLit of unit                        (* Ex3.16 *)
| ConsExp of exp * exp                  (* Ex3.16 *)
;;

type program =
  Exp of exp
| LetDecl of (id * exp) list list       (* Ex3.5 *)
| RecDecl of id * id * exp              (* Ex3.14 *)
;;

let counter = ref 0;;
let fresh_tyvar =                       (* Ex4.2 *)
  (* let counter = ref 0 in *)
  let body () =
    let v = !counter in
    counter := v + 1; v
  in body

let rec freevar_ty t =                 (* Ex4.2 *)
  match t with
  | TyInt ->
    MySet.empty
  | TyBool ->
    MySet.empty
  | TyVar t' ->
    MySet.singleton t'
  | TyFun (tv, te) ->
    MySet.union (freevar_ty tv) (freevar_ty te)
;;


let cleate_funexp x y = FunExp (x, y);;
let cleate_consexp x y = ConsExp (x, y);;

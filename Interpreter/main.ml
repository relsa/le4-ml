open Syntax
open Eval
open Typing

let rec read_eval_print env tyenv =
  (* 例外処理機構,及びprint_error関数をEx3.2で追加 *)
  let print_error s e te = 
    print_string (s);
    flush stdout;
    print_newline ();
    read_eval_print e te in
  print_string "# ";
  flush stdout;
  try
    let decl = Parser.toplevel Lexer.main (Lexing.from_channel stdin) in
    let (s, ty) = ty_decl tyenv decl in
    let (id, newenv, v) = eval_decl env decl in
    Printf.printf "val %s : " id;
    pp_ty ty;
    print_string " = ";
    pp_val v;
    print_newline();
    counter := 0;
    read_eval_print newenv tyenv
  with 
  | Eval.Error s ->
    print_error ("Eval error -- " ^ s) env tyenv
  | Parsing.Parse_error ->
    print_error "Parse error" env tyenv
  | Failure s ->
    print_error ("Failure -- " ^ s) env tyenv
  | Typing.Error s ->
    print_error ("Type error -- " ^ s) env tyenv
  (* | _ -> *)
  (*   print_error "Unknown Error" env tyenv *)
;;

let initial_env = 
  Environment.extend "i" (IntV 1)
    (* 以下、Ex3.1で追加*)
    (Environment.extend "ii" (IntV 2)
       (Environment.extend "iii" (IntV 3)
          (Environment.extend "iv" (IntV 4)
             (* 以上. *)
             (Environment.extend "v" (IntV 5) 
                (Environment.extend "x" (IntV 10) Environment.empty)))))


let initial_tyenv = 
  Environment.extend "i" TyInt
    (* 以下、Ex3.1で追加*)
    (Environment.extend "ii" TyInt
       (Environment.extend "iii" TyInt
	        (Environment.extend "iv" TyInt
	        (* 以上. *)
	           (Environment.extend "v" TyInt
		            (Environment.extend "x" TyInt Environment.empty)))))

let _ = read_eval_print initial_env initial_tyenv

{
let reservedWords = [
  (* Keywords *)
  ("else", Parser.ELSE);
  ("false", Parser.FALSE);
  ("if", Parser.IF);
  ("then", Parser.THEN);
  ("true", Parser.TRUE);
  ("in", Parser.IN);			(* Ex3.4 *)
  ("let", Parser.LET);			(* Ex3.4 *)
  ("and", Parser.AND);			(* Ex3.7 *)
  ("fun", Parser.FUN);			(* Ex3.8 *)
  ("rec", Parser.REC);			(* Ex3.14 *)
  ("match", Parser.MATCH);		(* Ex3.16 *)
  ("with", Parser.WITH);		(* Ex3.16 *)
] 
}

rule main = parse
  (* ignore spacing and newline characters *)
  [' ' '\009' '\012' '\n']+     { main lexbuf }

| "-"? ['0'-'9']+
    { Parser.INTV (int_of_string (Lexing.lexeme lexbuf)) }

| "(" { Parser.LPAREN }
| ")" { Parser.RPAREN }
| ";;" { Parser.SEMISEMI }
| "+" { Parser.PLUS }
| "*" { Parser.MULT }
| "<" { Parser.LT }
| "&&" { Parser.LAND }			(* Ex3.3 *)
| "||" { Parser.LOR }			(* Ex3.3 *)
| "=" { Parser.EQ }			(* Ex3.4 *)
| "->" { Parser.RARROW }		(* Ex3.7 *)
| "::" { Parser.CONS }			(* Ex3.16 *)
| "|" { Parser.BAR }			(* Ex3.16 *)
| "[" { Parser.LBR }			(* Ex3.16 *)
| "]" { Parser.RBR }			(* Ex3.16 *)
| ";" { Parser.SEMI }
| ['a'-'z'] ['a'-'z' '0'-'9' '_' '\'']*
    { let id = Lexing.lexeme lexbuf in
      try 
        List.assoc id reservedWords
      with
      _ -> Parser.ID id
     }
| eof { exit 0 }



open Syntax

type exval =
| IntV of int
| BoolV of bool
| ProcV of id * exp * dnval Environment.t ref (* Ex3.8 *)
| NilV of unit                                (* Ex3.16 *)
| ConsV of exval * exval                      (* Ex3.16 *)
and dnval = exval

exception Error of string

let err s = raise (Error s)

(* pretty printing *)
let rec string_of_exval =
  let rec string_of_list str list =     (* Ex3.16 *)
    match list with
    | NilV  _ -> str
    | ConsV (car, cdr) ->
      string_of_list (str ^ "; " ^ string_of_exval car) cdr
    | _ -> err ("Not a list") in
  function
  | IntV i -> string_of_int i
  | BoolV b -> string_of_bool b
  | ProcV (i, e, t) -> "fun"            (* Ex3.8 *)
  | NilV _ -> "[]"
  | ConsV (car, cdr) -> "[" ^ (string_of_list (string_of_exval car) cdr) ^  "]"
  | _ -> err ("This value cannot be converted to string")

let pp_val v = print_string (string_of_exval v)

let rec apply_prim op arg1 arg2 = match op, arg1, arg2 with
    Plus, IntV i1, IntV i2 -> IntV (i1 + i2)
  | Plus, _, _ -> err ("Both arguments must be integer: +")
  | Mult, IntV i1, IntV i2 -> IntV (i1 * i2)
  | Mult, _, _ -> err ("Both arguments must be integer: *")
  | Lt, IntV i1, IntV i2 -> BoolV (i1 < i2)
  | Lt, _, _ -> err ("Both arguments must be integer: <")
  (* 以下、Ex3.3で追加 *)
  | And, BoolV i1, BoolV i2 -> BoolV (i1 && i2)
  | And, _, _ -> err ("Both arguments must be boolean: &&")
  | Or, BoolV i1, BoolV i2 -> BoolV (i1 || i2)
  | Or, _, _ -> err ("Both arguments must be boolean: ||")
(* 以上 *)


let rec eval_exp env = function
Var x -> 
  (try Environment.lookup x env with 
    Environment.Not_bound -> err ("Variable not bound: " ^ x))
  | ILit i ->
    IntV i
  | BLit b ->
    BoolV b
  | NilLit _ ->                         (* Ex3.16 *)
    NilV ()
  | BinOp (op, exp1, exp2) ->
    let arg1 = eval_exp env exp1 in
    let arg2 = eval_exp env exp2 in
    apply_prim op arg1 arg2
  | IfExp (exp1, exp2, exp3) ->
    let test = eval_exp env exp1 in
    (match test with
      BoolV true -> eval_exp env exp2 
    | BoolV false -> eval_exp env exp3
    | _ -> err ("Test expression must be boolean: if"))

  (* | LetExp (id, exp1, exp2) -> (\* Ex3.4 *\) *)
  (*   let value = eval_exp env exp1 in *)
  (*   eval_exp (Environment.extend id value env) exp2 *)

  | LetExp (var, exp) ->               (* Ex3.7 *)
    let newenv = env_contains_vars var env env in
    eval_exp newenv exp

  | FunExp (id, exp) -> ProcV (id, exp, ref env) (* Ex3.8 *)

  (* | FunExp (param, exp) ->              (\* Ex3.10 *\) *)
  (*   begin *)
  (*     match param with *)
  (*     | car :: [] -> *)
  (*       ProcV (car, exp, ref env) *)
  (*     | car :: cdr -> *)
  (*       ProcV (car, FunExp (cdr, exp), ref env) (\* fun x -> expの形に *\) *)
  (*   end *)
  | AppExp (exp1, exp2) ->              (* Ex3.8 *)
    let funval = eval_exp env exp1 in
    let arg = eval_exp env exp2 in
    begin
      match funval with
      | ProcV (id, body, env') ->
        let newenv = Environment.extend id arg !env' in
        eval_exp newenv body
      | _ -> err ("Non-functional value is applied")
    end
  | LetRecExp (id, para, exp1, exp2) -> (* Ex3.14 *)
    let dummyenv = ref Environment.empty in
    let newenv = Environment.extend id (ProcV (para, exp1, dummyenv)) env in
    dummyenv := newenv;
    eval_exp newenv exp2
  | ConsExp (car, cdr) ->               (* Ex3.16 *)
    let carval = eval_exp env car in
    ConsV (carval, eval_exp env cdr)
  | MatchExp (list, case_nil, id1, id2, case_else) -> (* Ex3.16 *)
    let v = eval_exp env list in                      (* 適用するリストを評価 *)
    begin
      match v with
      | NilV _ -> eval_exp env case_nil 
      | ConsV (car, cdr) ->
        let newenv = Environment.extend id1 car env in (* carを環境に追加 *)
        let newenv' = Environment.extend id2 cdr newenv in (* cdrを環境に追加 *)
        eval_exp newenv' case_else
      | _ -> err ("Unmatched pattern is given")
    end

and env_contains_vars l env env'=
  match l with
    | [] -> env
    | car :: cdr ->
      let (id, exp) = car in
      let arg = eval_exp env' exp in
      let newenv = Environment.extend id arg env in
      Printf.printf "val %s = " id;
      pp_val arg;
      print_newline();
      env_contains_vars cdr newenv env'
;;

let rec eval_decl env = function
  | Exp e ->
    let v = eval_exp env e in ("-", env, v)
  | LetDecl decls ->
    begin match decls with
    | [] ->
      ("-", env, BoolV true)
    | car :: cdr ->
      let newenv = env_contains_vars car env env in
      eval_decl newenv (LetDecl cdr)
    end
  | RecDecl (id, para, exp) ->          (* Ex3.14 *)
    let dummyenv = ref Environment.empty in
    let newenv = Environment.extend id (ProcV (para, exp, dummyenv)) env in
    dummyenv := newenv;
    (id, newenv, ProcV(para, exp, ref newenv))
;;
